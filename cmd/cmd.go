package cmd

import (
	"math/rand"
	"plant_monitor/database"
	"plant_monitor/models"
	"time"
)

func ClearDatabase() {
	database.MI.DropAllCollections()
}

func CreateExampleDatabase() {
	sensorPool := []models.Sensor{
		{
			Name: "temperature",
			Unit: "°C",
		},
		{
			Name: "humidity",
			Unit: "%",
		},
		{
			Name: "insolation",
			Unit: "idk",
		},
		{
			Name: "humidityOfSoil",
			Unit: "%",
		},
	}
	for i := range sensorPool {
		sensorPool[i].Insert(database.MI)
	}
	plantPool := []models.Plant{
		{
			Type_: "Mango",
			Name:  "Monogoś",
			Location: models.Location{
				Latitude:  52.232473182550756,
				Longitude: 21.067181711311484,
			},
			Description: "",
			CustomID:    "Mango",
		},
		{
			Type_: "Banana",
			Name:  "Bananaś",
			Location: models.Location{
				Latitude:  52.232473182550756,
				Longitude: 21.067181711311484,
			},
			Description: "",
			CustomID:    "Banana",
		},
		{
			Type_: "Apple",
			Name:  "Appleś",
			Location: models.Location{
				Latitude:  52.232473182550756,
				Longitude: 21.067181711311484,
			},
			Description: "",
			CustomID:    "Apple",
		},
	}
	for i := range plantPool {
		plantPool[i].Insert(database.MI)
	}
	timestamp := time.Now().AddDate(0, -2, 0)
	var humidity float32 = 20.0
	humDirection := "up"
	for i := 0; i < 60*24; i++ {
		timestamp = timestamp.Add(time.Hour)
		if humDirection == "up" {
			humidity += 4.0 + rand.Float32()*4
			if humidity > 80 {
				humDirection = "down"
			}
		} else {
			humidity -= 4 + rand.Float32()*4
			if humidity < 20 {
				humDirection = "up"
			}
		}
		plantID := plantPool[rand.Intn(len(plantPool))].ID
		measurementPool := []models.Measurement{
			{
				// temperature
				Value:     rand.Float32()*10 + 23,
				Plant:     plantID,
				Sensor:    sensorPool[0].ID,
				Timestamp: timestamp,
			},
			{ // humidity
				Value:     humidity,
				Plant:     plantID,
				Sensor:    sensorPool[1].ID,
				Timestamp: timestamp,
			},
			{ //insolation
				Plant:     plantID,
				Value:     12,
				Sensor:    sensorPool[2].ID,
				Timestamp: timestamp,
			},
			{ // humidity of soil
				Value:     humidity - 4 + rand.Float32()*3,
				Plant:     plantID,
				Sensor:    sensorPool[3].ID,
				Timestamp: timestamp,
			},
		}

		for i := range measurementPool {
			measurementPool[i].Insert(database.MI)
		}
	}

	for i := range 22 {
		plantID := plantPool[rand.Intn(len(plantPool))].ID

		procedurePool := []models.Procedure{
			{
				Plant:     plantID,
				Procedure: "watering",
				Timestamp: time.Now().AddDate(0, -i, 0),
			},
			{
				Plant:     plantID,
				Procedure: "fertilization",
				Timestamp: time.Now().AddDate(0, -i, 0),
			},
		}
		for i := range procedurePool {
			procedurePool[i].Insert(database.MI)
		}
	}
}


func CreateProductionDatabase() {
	sensorPool := []models.Sensor{
		{
			Name: "temperature",
			Unit: "°C",
		},
		{
			Name: "humidity",
			Unit: "%",
		},
		{
			Name: "insolation",
			Unit: "idk",
		},
		{
			Name: "humidityOfSoil",
			Unit: "%",
		},
	}
	for i := range sensorPool {
		sensorPool[i].Insert(database.MI)
	}
	plantPool := []models.Plant{
		{
			Type_: "Magnolia",
			Name:  "Madzia",
			Location: models.Location{
				Latitude:  52.232473182550756,
				Longitude: 21.067181711311484,
			},
			Description: "",
			CustomID:    "Magnolia",
		},
	}
	for i := range plantPool {
		plantPool[i].Insert(database.MI)
	}
}
