# Plant monitor

## Run

### Requirements

* docker
* docker-compose

```bash
docker-compose up -d
go run .
```

# API endpoints

|Method  |Address                   |Description |   
|--------|-------------------       |--------------------------------|
|GET     |/api/v1/plant             | Get all plants from collection |
|GET     |/api/v1/plant/:id         | Get plant with id              |
|POST    |/api/v1/plant             | Add new plant                  |
|GET     |/api/v1/sensor            | get all sensors from collection |
|GET     |/api/v1/sensor/:id        | get sensor with id              |
|POST    |/api/v1/sensor            | add new sensor                  |
|GET     |/api/v1/measurement       | get all measurement from collection |
|GET     |/api/v1/measurement/:id   | get plant with id              |
|POST    |/api/v1/measurement       | add new measurement   |

example request

```json
{
  "plant_id": "617f6c175c2b064f4d626896",
  "measurements": [
    {
      "sensor_name": "humidity",
      "value": 23.43,
      "date": "11.11.2021"
    },
    {
      "sensor_name": "temperature",
      "value": 43.21,
      "date": "11.11.2021"
    }
  ]
}
```