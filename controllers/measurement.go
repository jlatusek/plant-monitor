package controllers

import (
	"encoding/json"
	"errors"
	"log"
	"plant_monitor/api"
	"plant_monitor/database"
	"plant_monitor/models"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

func addMeasurement(body []byte) ([]*mongo.InsertOneResult, error) {
	measurementPoolMsg := &api.MeasurementPoolMsg{}
	if err := json.Unmarshal(body, measurementPoolMsg); err != nil {
		log.Println("Couldn't unmarshal json")
		return nil, errors.New("couldn't unmarshal json")
	}
	insertedIdPool := make([]*mongo.InsertOneResult, len(measurementPoolMsg.Measurement))
	for i := range measurementPoolMsg.Measurement {
		measurementApi := measurementPoolMsg.Measurement[i]
		measurement, err := models.MeasurementFromApiMeasurement(&measurementApi, measurementPoolMsg.PlantId, database.MI)
		if err != nil {
			return nil, errors.New("could not create measurement from measurement api")
		}
		insertedIdPool[i] = measurement.Insert(database.MI)
	}
	return insertedIdPool, nil
}

func AddMeasurement(c *fiber.Ctx) error {
	idPool, err := addMeasurement(c.Body())
	if err != nil {
		return c.JSON(idPool)
	}
	return err
}

func GetMeasurement(c *fiber.Ctx) error {
	plant, err := models.GetMeasurementById(database.MI, c.Params("id"))
	if err != nil {
		return c.SendString(err.Error())
	}
	return c.JSON(plant)
}
func GetMeasurementAll(c *fiber.Ctx) error {
	sensorName := c.Query("sensor", "all")
	afterDateStr := c.Query("afterdate", "")
	if sensorName == "all" {
		measurements, err := models.GetMeasurementAll(database.MI)
		if err != nil {
			return c.SendString(err.Error())
		}
		return c.JSON(measurements)
	}
	sensor, err := models.GetSensorByName(database.MI, sensorName)
	if err != nil {
		return c.SendString(err.Error())
	}
	var measurements []models.Measurement
	if afterDateStr != "" {
		afterDate, err := time.Parse(time.RFC3339, afterDateStr)
		if err != nil {
			return c.SendString("Wrong date format")
		}
		measurements, _ = models.GetMeasurementsListForSensorAfterDate(database.MI, sensor.ID, afterDate)
	} else {
		measurements, _ = models.GetMeasurementsListForSensor(database.MI, sensor.ID)
	}
	return c.JSON(measurements)
}

func GetLastMeasurement(c *fiber.Ctx) error {
	sensorName := c.Query("sensor", "all")
	sensor, err := models.GetSensorByName(database.MI, sensorName)
	if err != nil {
		return c.SendString(err.Error())
	}
	var measurements models.Measurement
	measurements, _ = models.GetLastMeasurements(database.MI, sensor.ID)
	return c.JSON(measurements)
}
