package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"plant_monitor/api"
	"plant_monitor/database"
	"plant_monitor/models"
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestAddMeasurement(t *testing.T) {
	database.InitTestDb()
	plant := models.Plant{
		Type_: "Awokado",
		Name:  "Helenka",
		Location: models.Location{
			Latitude:  1,
			Longitude: 2,
		},
		Description: "Mój kochany kwiatuszek",
	}
	plant.Insert(database.MI)
	measurement := api.MeasurementPoolMsg{
		PlantId: plant.ID.Hex(),
		Measurement: []api.MeasurementMsg{
			{
				SensorName: "Temperature",
				Value:      23,
			},
		},
	}
	sensor := models.Sensor{
		Name: "Temperature",
		Unit: "Sth",
	}
	sensor.Insert(database.MI)
	out, err := json.Marshal(&measurement)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}
	_, err = addMeasurement(out)
	if err != nil {
		t.Fatal(err)
	}
}

func TestAddMeasurementController(t *testing.T) {
	app := fiber.New()
	database.InitTestDb()
	sensor := models.Sensor{
		Name: "Temperature",
		Unit: "Sth",
	}
	sensor.Insert(database.MI)
	plant := models.Plant{
		Type_: "Awokado",
		Name:  "Helenka",
		Location: models.Location{
			Latitude:  1,
			Longitude: 2,
		},
		Description: "Mój kochany kwiatuszek",
	}
	plant.Insert(database.MI)
	measurement := api.MeasurementPoolMsg{
		PlantId: plant.ID.Hex(),
		Measurement: []api.MeasurementMsg{
			{
				SensorName: "Temperature",
				Value:      23,
			},
		},
	}
	out, err := json.Marshal(&measurement)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}

	app.Post("/test", AddMeasurement)
	resp, err := app.Test(httptest.NewRequest("POST", "/test", bytes.NewBuffer(out)))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatalf(fmt.Sprintf("Wrong return code: %d", resp.StatusCode))
	}

}
