package controllers

import (
	"encoding/json"
	"plant_monitor/database"
	"plant_monitor/models"
	"github.com/gofiber/fiber/v2"
)

func AddProcedure(c *fiber.Ctx) error {
	body := c.Body()
	var procedure models.Procedure
	err := json.Unmarshal(body, &procedure)
	if err != nil {
		return err
	}
	procedure.Insert(database.MI)
	return nil
}

func GetAllProcedures(c *fiber.Ctx) error {
	plants, err := models.GetProcedureAll(database.MI)
	if err != nil {
		return c.SendString(err.Error())
	}
	return c.JSON(plants)
}
