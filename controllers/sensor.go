package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"plant_monitor/database"
	"plant_monitor/models"
)

func GetSensor(c *fiber.Ctx) error {
	plant, err := models.GetSensorById(database.MI, c.Params("id"))
	if err != nil {
		return c.SendString(err.Error())
	}
	return c.JSON(plant)
}
func GetSensorAll(c *fiber.Ctx) error {
	plants, err := models.GetSensorAll(database.MI)
	if err != nil {
		return c.SendString(err.Error())
	}
	return c.JSON(plants)
}

func addSensor(body []byte) error {
	var sensor models.Sensor
	err := json.Unmarshal(body, &sensor)
	if err != nil {
		return err
	}
	sensor.Insert(database.MI)
	return nil
}

func AddSensor(c *fiber.Ctx) error {
	return addSensor(c.Body())
}
