package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"plant_monitor/database"
	"plant_monitor/models"
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestAddSensor(t *testing.T) {
	database.InitTestDb()
	sensor := models.Sensor{
		Name: "Temperature",
		Unit: "sth",
	}
	out, err := json.Marshal(&sensor)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}
	err = addSensor(out)
	if err != nil {
		t.Fatal(err)
	}
}

func TestAddSensorController(t *testing.T) {
	app := fiber.New()
	database.InitTestDb()
	sensor := models.Sensor{
		Name: "Temperature",
		Unit: "sth",
	}
	out, err := json.Marshal(&sensor)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}

	app.Post("/test", AddSensor)
	resp, err := app.Test(httptest.NewRequest("POST", "/test", bytes.NewBuffer(out)))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatalf(fmt.Sprintf("Wrong return code: %d", resp.StatusCode))
	}

}
