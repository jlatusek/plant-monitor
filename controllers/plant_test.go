package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http/httptest"
	"plant_monitor/database"
	"plant_monitor/models"
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestAddPlant(t *testing.T) {
	database.InitTestDb()
	plant := models.Plant{
		Type_:       "Palma",
		Name:        "Helena",
		Location:    models.Location{
			Latitude:  23,
			Longitude: 43,
		},
		Description: "Is beautiful",
	}
	out, err := json.Marshal(&plant)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}
	err = addPlant(out)
	if err != nil {
		t.Fatal(err)
	}
}

func TestAddPlantController(t *testing.T) {
	app := fiber.New()
	database.InitTestDb()
	plant := models.Plant{
		Type_:       "Palma",
		Name:        "Helena",
		Location:    models.Location{
			Latitude:  23,
			Longitude: 43,
		},
		Description: "Is beautiful",
	}
	out, err := json.Marshal(&plant)
	if err != nil {
		t.Fatal("Error during serializing measurement ")
	}

	app.Post("/test", AddPlant)
	resp, err := app.Test(httptest.NewRequest("POST", "/test", bytes.NewBuffer(out)))
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatalf(fmt.Sprintf("Wrong return code: %d", resp.StatusCode))
	}

}
