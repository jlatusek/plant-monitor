package api

type MeasurementMsg struct {
	SensorName string  `json:"sensor_name"`
	Value      float32 `json:"value"`
}

type MeasurementPoolMsg struct {
	Measurement []MeasurementMsg `json:"measurements"`
	PlantId     string           `json:"plant_id"`
}
