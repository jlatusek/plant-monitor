package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"plant_monitor/cmd"
	"plant_monitor/configuration"
	"plant_monitor/controllers"
	"plant_monitor/database"

	"github.com/gofiber/fiber/v2"
)

func middleware(c *fiber.Ctx) error {
	return c.Next()
}

type Args struct {
	createDb *bool
	createProdDb *bool
	clearDb  *bool
	envPath  *string
}

func parseArguments() Args {
	args := Args{
		createDb: flag.Bool("create-db", false, "Create Example DB"),
		clearDb:  flag.Bool("clear-db", false, "Clear Example DB"),
		createProdDb: flag.Bool("create-prod-db", false, "Create Production DB"),
		envPath:  flag.String("env", "", "Environment settings file"),
	}
	flag.Parse()
	return args
}

func main() {
	arguments := parseArguments()
	if len(*arguments.envPath) <= 0 {
		*arguments.envPath = ".env"
		log.Println("No env file provided - use default .env")
	}
	configuration.ServerConfiguration.Load(*arguments.envPath)
	configuration.ServerConfiguration.Print()
	database.MI.Connect()
	database.MI.ListDatabases()
	if *arguments.createDb {
		cmd.CreateExampleDatabase()
		os.Exit(0)
	} else if *arguments.createProdDb {
		cmd.CreateProductionDatabase()
		os.Exit(0)
	} else if *arguments.clearDb {
		cmd.ClearDatabase()
		os.Exit(0)
	}

	app := fiber.New()
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World 👋!")
	})
	api := app.Group("/api", middleware)
	v1 := api.Group("/v1", middleware)

	plant := v1.Group("plant", middleware)
	plant.Get("/", controllers.GetPlantAll)
	plant.Get("/:id", controllers.GetPlant)
	plant.Post("/", controllers.AddPlant)

	measurement := v1.Group("measurement", middleware)
	measurement.Get("/", controllers.GetMeasurementAll)
	measurement.Get("/byID/:id", controllers.GetMeasurement)
	measurement.Get("/last", controllers.GetLastMeasurement)
	measurement.Post("/", controllers.AddMeasurement)

	sensor := v1.Group("sensor", middleware)
	sensor.Get("/", controllers.GetSensorAll)
	sensor.Get("/:id", controllers.GetSensor)
	sensor.Post("/", controllers.AddSensor)

	procedure := v1.Group("procedure", middleware)
	procedure.Get("/", controllers.GetAllProcedures)
	procedure.Post("/", controllers.AddProcedure)

	err := app.Listen(fmt.Sprint(":", configuration.ServerConfiguration.Port))
	if err != nil {
		log.Println(err)
	}
	database.MI.Disconnect()
}
