package models

import (
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"plant_monitor/database"
)

func (sensor *Sensor) Insert(db database.MongoInstance) *mongo.InsertOneResult {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	insertResult, err := db.Db.Collection("Sensor").InsertOne(ctx, sensor)
	if err != nil {
		panic(err)
	}
	if sensor.ID.IsZero() {
		sensor.ID = insertResult.InsertedID.(primitive.ObjectID)
	}
	return insertResult
}

func GetSensorById(db database.MongoInstance, id string) (*Sensor, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
		return nil, errors.New("wrong id value")
	}
	var sensor Sensor
	if err = db.Db.Collection("Sensor").FindOne(ctx, bson.M{"_id": objectId}).Decode(&sensor); err != nil {
		log.Println(err)
		return nil, errors.New("sensor with id not exist")
	}
	return &sensor, nil
}

func GetSensorByName(db database.MongoInstance, name string) (*Sensor, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	var sensor Sensor
	if err := db.Db.Collection("Sensor").FindOne(ctx, bson.M{"name": name}).Decode(&sensor); err != nil {
		log.Println(err)
		return nil, errors.New("sensor with id not exist")
	}
	return &sensor, nil
}

func GetSensorAll(db database.MongoInstance) ([]Sensor, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	cursor, err := db.Db.Collection("Sensor").Find(ctx, bson.D{})
	if err != nil {
		log.Println(err)
		return nil, err
	}
	sensors := make([]Sensor, 0)
	for i := 0; cursor.Next(ctx); i++ {
		sensors = append(sensors, Sensor{})
		var episode bson.M
		if err = cursor.Decode(&sensors[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return sensors, nil
}
