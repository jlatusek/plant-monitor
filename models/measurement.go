package models

import (
	"errors"
	"fmt"
	"log"
	"plant_monitor/api"
	"plant_monitor/database"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func MeasurementFromApiMeasurement(measurementApi *api.MeasurementMsg, plantId string, db database.MongoInstance) (*Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	var sensor Sensor
	if err := db.Db.Collection("Sensor").FindOne(ctx, bson.M{"name": measurementApi.SensorName}).Decode(&sensor); err != nil {
		log.Println(err)
		log.Printf("Sensor with type: %s  does not exist", measurementApi.SensorName)
	}

	var plant Plant
	if err := db.Db.Collection("Plant").FindOne(ctx, bson.M{"custom_id": plantId}).Decode(&plant); err != nil {
		log.Println(err)
		log.Printf("Plant with custom id: %s  does not exist", plantId)
	}

	var measurement Measurement = Measurement{
		Timestamp:     time.Now(),
		Sensor:        sensor.ID,
		Value:         measurementApi.Value,
		Plant:         plant.ID,
		CustomPlantID: plantId,
	}
	return &measurement, nil
}

func (measurement *Measurement) Insert(db database.MongoInstance) *mongo.InsertOneResult {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	insertResult, err := db.Db.Collection("Measurement").InsertOne(ctx, measurement)
	if err != nil {
		panic(err)
	}
	if measurement.ID.IsZero() {
		measurement.ID = insertResult.InsertedID.(primitive.ObjectID)
	}
	return insertResult
}

func GetMeasurementById(db database.MongoInstance, id string) (*Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
		return nil, errors.New("wrong id value")
	}
	var measurement Measurement
	if err = db.Db.Collection("Measurement").FindOne(ctx, bson.M{"_id": objectId}).Decode(&measurement); err != nil {
		log.Println(err)
		return nil, errors.New("measurement with id not exist")
	}
	return &measurement, nil
}

func GetMeasurementAll(db database.MongoInstance) ([]Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{Key: "timestamp", Value: -1}})
	cursor, err := db.Db.Collection("Measurement").Find(ctx, bson.D{}, findOptions)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	measurements := make([]Measurement, 0)
	for i := 0; cursor.Next(ctx); i++ {
		measurements = append(measurements, Measurement{})
		var episode bson.M
		if err = cursor.Decode(&measurements[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return measurements, nil
}

func GetMeasurementsListForSensor(db database.MongoInstance, sensorID primitive.ObjectID) ([]Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{Key: "timestamp", Value: -1}})
	cursor, err := db.Db.Collection("Measurement").Find(ctx, bson.M{"sensor": sensorID}, findOptions)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	measurements := make([]Measurement, 0)
	for i := 0; cursor.Next(ctx); i++ {
		measurements = append(measurements, Measurement{})
		var episode bson.M
		if err = cursor.Decode(&measurements[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return measurements, nil
}

func GetMeasurementsListForSensorAfterDate(db database.MongoInstance, sensorID primitive.ObjectID, timestamp time.Time) ([]Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	findOptions := options.Find()
	findOptions.SetSort(bson.D{{Key: "timestamp", Value: -1}})
	cursor, err := db.Db.Collection("Measurement").Find(ctx, bson.M{"sensor": sensorID, "timestamp": bson.M{"$gt": timestamp}}, findOptions)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	measurements := make([]Measurement, 0)
	for i := 0; cursor.Next(ctx); i++ {
		measurements = append(measurements, Measurement{})
		var episode bson.M
		if err = cursor.Decode(&measurements[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return measurements, nil
}

func GetLastMeasurements(db database.MongoInstance, sensorID primitive.ObjectID) (Measurement, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	findOptions := options.FindOne()
	findOptions.SetSort(bson.D{{Key: "timestamp", Value: -1}})
	collection := db.Db.Collection("Measurement").FindOne(ctx, bson.M{"sensor": sensorID}, findOptions)

	var measurement Measurement
	err := collection.Decode(&measurement)
	if err != nil {
		log.Fatal(err)
	}
	return measurement, nil
}
