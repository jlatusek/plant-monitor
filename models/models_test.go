package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"plant_monitor/database"
	"testing"
	"time"
)

func TestDbPlantInsert(t *testing.T) {
	database.InitTestDb()
	plant := Plant{
		Type_: "Flower",
		Name:  "Helena",
		Location: Location{
			Latitude:  21.32,
			Longitude: 32.65,
		},
		Description: "My beautiful helena",
	}
	res := plant.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during Plant insertion")
	}
}

func TestDbMeasurementInsertGet(t *testing.T) {
	database.InitTestDb()
	measurement := Measurement{
		Timestamp: time.Now(),
		Sensor:    primitive.NewObjectID(),
		Value:     23,
	}
	res := measurement.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during Measurement insertion")
	}
}

func TestDbMeasurementGetAfterDate(t *testing.T) {
	database.InitTestDb()
	sensor := primitive.NewObjectID()
	measurement1 := Measurement{
		Timestamp: time.Now(),
		Sensor:    sensor,
		Value:     23,
	}
	measurement2 := Measurement{
		Timestamp: time.Now().AddDate(0, -2, 0),
		Sensor:    sensor,
		Value:     23,
	}
	res := measurement1.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during Measurement insertion")
	}
	res = measurement2.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during Measurement insertion")
	}

	resMesurement, err := GetMeasurementsListForSensorAfterDate(database.MI, sensor, time.Now().AddDate(0, -1, 0))
	if err != nil {
		t.Fatal("Error during object receiving")
	}
	if resMesurement[0] != measurement1 {
		t.Fatal("Object suck")
	}
	if len(resMesurement) != 1 {
		t.Fatal("Received more than one object")
	}
}

func TestDbMeasurementNoTimestampInsertGet(t *testing.T) {
	database.InitTestDb()
	measurement := Measurement{
		Sensor: primitive.NewObjectID(),
		Value:  23,
	}
	res := measurement.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during Measurement insertion")
	}
}

func TestDbSensorInsertGet(t *testing.T) {
	database.InitTestDb()
	sensor := Sensor{
		Name: "Air Humidity",
		Unit: "%",
	}
	res := sensor.Insert(database.MI)
	if res.InsertedID == nil {
		t.Fatal("Error during sensor insertion")
	}
	id := res.InsertedID.(primitive.ObjectID).Hex()
	retSensor, err := GetSensorById(database.MI, id)
	if err != nil {
		t.Fatal(err)
	}
	if *retSensor != sensor {
		t.Fatal("Sensors not equal")
	}
}
