package models

import (
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"plant_monitor/database"
)

func (plant *Plant) Insert(db database.MongoInstance) *mongo.InsertOneResult {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	insertResult, err := db.Db.Collection("Plant").InsertOne(ctx, plant)
	if err != nil {
		panic(err) // TODO poprawić
	}
	if plant.ID.IsZero() {
		plant.ID = insertResult.InsertedID.(primitive.ObjectID)
	}
	return insertResult
}

func GetPlantById(db database.MongoInstance, id string) (*Plant, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	objectId, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println(err)
		return nil, errors.New("wrong id value")
	}
	var plant Plant
	if err = db.Db.Collection("Plant").FindOne(ctx, bson.M{"_id": objectId}).Decode(&plant); err != nil {
		log.Println(err)
		return nil, errors.New("plant with id not exist")
	}
	return &plant, nil
}

func GetPlantAll(db database.MongoInstance) ([]Plant, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	cursor, err := db.Db.Collection("Plant").Find(ctx, bson.D{})
	if err != nil {
		log.Println(err)
		return nil, err
	}
	plants := make([]Plant, 0)
	for i := 0; cursor.Next(ctx); i++ {
		plants = append(plants, Plant{})
		var episode bson.M
		if err = cursor.Decode(&plants[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return plants, nil
}
