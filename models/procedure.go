package models

import (
	"errors"
	"fmt"
	"log"
	"plant_monitor/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var collectionName = "Procedure"

func (procedure *Procedure) Insert(db database.MongoInstance) *mongo.InsertOneResult {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	insertResult, err := db.Db.Collection(collectionName).InsertOne(ctx, procedure)
	if err != nil {
		panic(err) // TODO poprawić
	}
	if procedure.ID.IsZero() {
		procedure.ID = insertResult.InsertedID.(primitive.ObjectID)
	}
	return insertResult
}

func GetProcedureAll(db database.MongoInstance) ([]Procedure, error) {
	ctx, cancel := db.GetControllerContext()
	defer cancel()
	cursor, err := db.Db.Collection(collectionName).Find(ctx, bson.D{})
	if err != nil {
		log.Println(err)
		return nil, err
	}
	procedure := make([]Procedure, 0)
	for i := 0; cursor.Next(ctx); i++ {
		procedure = append(procedure, Procedure{})
		var episode bson.M
		if err = cursor.Decode(&procedure[i]); err != nil {
			log.Println(err)
			return nil, errors.New("decode object problem")
		}
		fmt.Println(episode)
	}
	err = cursor.Close(ctx)
	if err != nil {
		log.Fatal(err)
	}
	return procedure, nil
}
